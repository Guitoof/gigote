#!/usr/bin/python2

import OSC
import time
import random

# Init OSC
client = OSC.OSCClient()
msg = OSC.OSCMessage()

msg.setAddress("/Python/volume")
    
for i in range(10):
    time.sleep(1)
    msg.append(random.random())
    try:
        client.sendto(msg, ('127.0.0.1', 5005))
        msg.clearData()
    except:
        print 'Connection refused'
        pass
    
    
