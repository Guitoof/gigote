#!/usr/bin/python2

import OSC
import threading
import sys
import time
import win32api

#------OSC Server-------------------------------------#
receive_address = '127.0.0.1', 5004

# OSC Server. there are three different types of server. 
s = OSC.ThreadingOSCServer(receive_address)

# this registers a 'default' handler (for unmatched messages)
s.addDefaultHandlers()

# define a message-handler function for the server to call.
def printing_handler(addr, tags, stuff, source):
    if addr=='/test':
        print "Test", stuff
    elif addr=='/meta':
        print "Meta", stuff
    elif addr=='/meta/sessionName':
        print "Name", stuff
    elif addr=='/tempo':
        print "Tempo", stuff
    elif addr=='/isPlaying':
        print "isPlaying", stuff
    elif addr=='/currentBeat':
        print "Beat", stuff

s.addMsgHandler("/test", printing_handler)
s.addMsgHandler("/meta", printing_handler)
s.addMsgHandler("/tempo", printing_handler)
s.addMsgHandler("/isPlaying", printing_handler)
s.addMsgHandler("/meta/sessionName", printing_handler)
s.addMsgHandler("/currentBeat", printing_handler)


def main():
    # Start OSCServer
    print "Starting OSCServer"
    st = threading.Thread(target=s.serve_forever)
    st.start()
    while True:
        current = win32api.GetCursorPos()
        cx = sx = current[0]
        cy = sy = current[1]
        print "Mouse", cx, cy, "."
        time.sleep(0.1)

main()
