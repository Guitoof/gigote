__author__ = 'Guillaume'

import time
import json
import os
import numpy as np

class Recorder(object):

    def new_file(self,file_name):
        try:
           self.file.close()
        except:
            pass
        self.t0 = time.time()

        full_name = self.path + file_name

        self.file = open(full_name,'a')
        json.dump({ "t0": self.t0},self.file)
        self.file.write("\n")

    def record_data(self,raw_data):
        if raw_data:
            t = time.time() - self.t0
            obj = { "t": t, "data": raw_data}
            json.dump(obj, self.file)
            self.file.write("\n")


    def close(self):
        self.file.close()

    def set_path(self,path):
        self.path = path



class Reader(object):

    def open_file(self,file_name):
        try:
           self.file.close()
        except:
            pass
        self.file = open(file_name,'r')
        obj_init = json.loads(self.file.readline())

        t0 = obj_init['t0']
        duration = 0.
        for line in self.file:
            obj = json.loads(line)
            if 't' in obj:
                if obj['t'] > duration: duration = obj['t']

        return t0,duration

    def get_session_list(self,path,name):
        """
        Get a list of session folder to explore related to a particular name.
        :param path:
        :param name:
        :return:
        """
        dir_list = os.listdir(path)

        session_list = []

        for file_name in dir_list:
            idx = file_name.find(name)
            if idx == 0:
                session_list.append(file_name)

        return session_list

    def init_size_vector_size(self,vectoriser,file_name1,file_name2=''):
        """
        Init the vector length in the vectoriser object.
        :param file_name:
        :param vectoriser:
        :return:
        """
        t0,duration = self.open_file(file_name1)

        tend = t0 + duration
        durationb = duration
        tendb = tend
        if file_name2:
            t0b,durationb = self.open_file(file_name2)
            tendb = t0b + durationb


        t0 = min(t0,t0b)
        tend = max(tend,tendb)

        duration = tend - t0

        vectoriser.init(t0,duration)


    def vectorise_from_file(self,file_name,vc):
        """
        Set in the vectoriser object the full vectors information got from the file.
        :param file_name:
        :param vc:
        :return:
        """
        self.open_file(file_name)

        self.file.seek(0)
        for line in self.file:
            obj = json.loads(line)
            #Vectorise input and output and feel the late NaN numbers after OSc instance

            if 't' in obj:
                vc.t = obj['t']

            vc.vectorise_input(obj)

            vc.vectorise_output(obj)
            vc.t_last = vc.t

        vc.assign_values(vc.input,0,vc.n_inputs,vc.iterate_copy,vc.duration)
        vc.assign_values(vc.output,0,vc.n_outputs,vc.iterate_copy,vc.duration)


        try:
            if vc.check_vecorisation_undone():
                raise ValueError
        except ValueError:
                print "Input and Output vector are not filled. NaN entries still remain."




class Vectoriser(object):

    def __init__(self,FS):

        self.FS = FS
        self.dt = 1./FS

    def init_online(self,t0):
        self.init(t0,self.dt)

    def init(self,t0,duration):
        self.init_io()
        self.t0 = t0

        self.t = 0
        self.t_last = 0

        self.duration = float(duration)
        self.tend = self.t0 + self.duration
        self.n_time = int( self.duration/ self.dt)+1

        #
        self.input = np.empty((self.n_inputs,self.n_time))
        self.output = np.empty((self.n_outputs,self.n_time))
        self.input[:] = np.NaN
        self.output[:] = np.NaN

        self.input[:,0] = self.default_input_vector
        self.output[:,0] = self.default_output_vector


    def get_time_idx(self,t):
        return int(np.trunc(t/self.dt))


    def get_time_from_idx(self,t_idx):
        return self.t0 + t_idx*self.dt


    def check_vecorisation_undone(self):
        return np.isnan(self.input).any() or np.isnan(self.output).any()


    def assign_values(self,vector,field_start,field_number,iterate_func,t=-1):
        """
        Generic function for assigning all values since last reception.
        It is compatible in online mode to refresh the current step when the input is one dimensional.

        :param vector: input or output vector
        :param fields: fields of the vector
        :param iterate_func: iterating function
        :param t: time of current received object
        """
        if t == -1:
            t= self.t

        current_t_idx = self.get_time_idx(t)
        field_end = field_start+field_number
        t_idx = current_t_idx

        # Find the last element we know in the recorded vector
        while t_idx > 0 and np.isnan(vector[field_start:field_end,t_idx]).all():
            t_idx -= 1

        # get the value to update
        val = vector[field_start:field_end,t_idx]

        t_last = self.t_last
        # climb back, up to the current index and store updated values
        for idx in range(t_idx,current_t_idx+1):

            t_next = t-self.dt*(current_t_idx - idx)
            val = iterate_func(val,t_last,t_next)
            vector[field_start:field_end,idx] = val

    def iterate_copy(self,val,t_last,t_next):
        """
        Passive function to copy the past value of the vector to next entry.
        :param val:
        :param t_idx:
        :return:
        """
        return val