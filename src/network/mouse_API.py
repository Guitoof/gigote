__author__ = 'Guillaume'

from ctypes import windll, Structure, c_ulong, byref
import numpy as np
from file_recording_management import Vectoriser

class POINT(Structure):
    _fields_ = [("x", c_ulong), ("y", c_ulong)]

class Mouse_recorder(object):

    def __init__(self):
        pass

    def get_meta(self):
        point0 = self.get_data()
        return { "point0": point0 }

    def get_data(self):
        pt = POINT()
        windll.user32.GetCursorPos(byref(pt))
        return { "x": pt.x, "y": pt.y}



class MouseVectoriser(Vectoriser):

    def init_io(self):
        self.n_inputs = 2
        self.n_outputs = 0

        self.default_input_vector = [0,0]
        self.default_output_vector = []

    def vectorise_input(self,obj):
        """
        Fill up the input vector using the
        :param obj:
        :return:
        """
        if 'data' in obj:
            self.assign_values(self.input,0,2,self.iterate_copy)
            self.input[:,self.get_time_idx(obj['t'])] = np.array([obj['data']['x'],obj['data']['y']])


    def vectorise_output(self,obj):
        pass

