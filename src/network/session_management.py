__author__ = 'Guillaume'

from os import path,mkdir
from time import strftime, gmtime, time
import json

#class Session_meta(object):
#    def __init__(self):
#        self.name = ""
#        self.type = ""
#        self.user = ""
#        self.date = ""
#        self.day_time = ""
#        self.date_second = 0
#
#        self.osc_meta = {}
#        self.controller_meta = {}

class Session(object):

    def __init__(self,Name,Type,User):
        self.meta = lambda: None

        self.meta.name = Name
        self.meta.type = Type
        self.meta.user = User
        self.meta.date = strftime("%d_%b_%Y", gmtime())
        self.meta.day_time = strftime("%H_%M_%S", gmtime())
        self.meta.second = time()
        self.meta.mode = ''

        self.id = 0
        self.set_path()

    def get_short_name(self):
        return self.meta.name + ("%d" % self.id).zfill(5)


    def set_path(self):
        while path.exists(self.get_short_name() + "/"):
            self.id += 1

        self.path = self.get_short_name() + "/"
        mkdir(self.path)

        return self.path

    def save(self):
        full_name = self.path + 'session.json'

        self.file = open(full_name,'w')
        json.dump(self.meta.__dict__,self.file)
        self.file.close()


    def sync_osc(self,osc_receiver,osc_recorder):
        osc_meta = osc_receiver.get_meta()
        self.meta.osc_meta = osc_meta

        osc_recorder.set_path(self.path)
        osc_recorder.new_file(self.get_short_name() + ".osc")



    def sync_mouse(self,osc_receiver,osc_recorder):
        mouse_meta = osc_receiver.get_meta()
        self.meta.mouse_meta = mouse_meta

        osc_recorder.set_path(self.path)
        osc_recorder.new_file(self.get_short_name() + ".mouse")