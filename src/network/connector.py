__author__ = 'Guillaume'


from session_management import *
from mouse_API import *
from osc_API import *
from file_recording_management import *

from sklearn import linear_model
import numpy as np
import OSC

class MouseToLiveConnector(object):

    def __init__(self,ip,port,FS=100):

        self.FS = 100

        self.osc_receiver = OSCreceiver(ip,port)
        self.osc_recorder = Recorder()
        self.osc_vectoriser = OscVectoriser(FS)

        self.mouse_receiver = Mouse_recorder()
        self.mouse_recorder = Recorder()
        self.mouse_vectoriser = MouseVectoriser(FS)

        self.classifier = linear_model.LinearRegression()
        self.new_session()

    def new_session(self):
        self.session = Session('test_MouseToLive','[osc,mouse]','Guillaume')
        self.session.sync_osc(self.osc_receiver,self.osc_recorder)
        self.session.sync_mouse(self.mouse_receiver,self.mouse_recorder)
        self.session.save()


    def serve(self,duration):
        """
        Run a server that captures both mouse and OSC data
        :param duration
        """



        t0 = time.time()
        self.mouse_vectoriser.init_online(t0)
        self.osc_vectoriser.init_online(t0)


        t = t0
        print "Start serving OSC + Mouse."
        while t - t0 < duration:
            # update time stamp in vectoriser
            t = time.time()
            self.mouse_vectoriser.t = t
            self.osc_vectoriser.t = t

            # get and record mouse data
            mouse_data = self.mouse_receiver.get_data()
            self.mouse_recorder.record_data(mouse_data)

            # update state vector for instant classification
            self.mouse_vectoriser.vectorise_input(mouse_data)


            # get and record osc data
            #try:
            data = self.osc_receiver.get_data()

            self.osc_recorder.record_data(data)
            self.osc_receiver.process_data(self,data)

            # update state vector for instance classification
            self.osc_vectoriser.vectorise_input(data)

            #except socket.error:
            #    pass
            X = self.mouse_vectoriser.input
            np.vstack(X,self.osc_vectoriser.input)
            np.vstack(X,[1])


            print self.classifier.predict(X.T)


            time.sleep(0.01)

        self.osc_recorder.file.close()
        self.mouse_recorder.file.close()

        print "Stop serving."

    def train_classifier(self,name):
        """
        Train the classifier that will classify along the output
        :param name:
        :return:
        """
        FS = self.FS

        osc_vectoriser = OscVectoriser(self.FS)
        osc_file_name = name + '/' + name + '.osc'

        mouse_vectoriser = MouseVectoriser(FS)
        mouse_file_name = name + '/' + name + '.mouse'

        reader = Reader()
        reader.init_size_vector_size(osc_vectoriser,osc_file_name,mouse_file_name)
        reader.init_size_vector_size(mouse_vectoriser,osc_file_name,mouse_file_name)

        reader.vectorise_from_file(osc_file_name,osc_vectoriser)
        reader.vectorise_from_file(mouse_file_name,mouse_vectoriser)

        self.classifier = linear_model.LinearRegression()
        X = mouse_vectoriser.input
        Y = mouse_vectoriser.output
        np.vstack((X,osc_vectoriser.input))
        np.vstack((X,np.ones(1,mouse_vectoriser.n_time)))
        np.vstack((Y,osc_vectoriser.output))

        self.classifier.fit (X.T,Y.T)

        # The mean square error
        print("Residual sum of squares: %.2f"
              % np.mean((self.classifier.predict(X.T) - Y.T) ** 2))
        # Explained variance score: 1 is perfect prediction
        print('Variance score: %.2f' % self.classifier.score(X.T, Y.T))