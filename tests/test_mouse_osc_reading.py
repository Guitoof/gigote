__author__ = 'Guillaume'

import src.network.file_recording_management as f_mgmt
import src.network.osc_API as osc_API
import src.network.mouse_API as mouse_API
import os

FS = 2

print os.path.abspath(__file__)
name = 'test_MouseToLive00019'

osc_vectoriser = osc_API.OscVectoriser(FS)
osc_file_name = name + '/' + name + '.osc'

mouse_vectoriser = mouse_API.MouseVectoriser(FS)
mouse_file_name = name + '/' + name + '.mouse'

reader = f_mgmt.Reader()
reader.init_size_vector_size(osc_vectoriser,osc_file_name,mouse_file_name)
reader.init_size_vector_size(mouse_vectoriser,osc_file_name,mouse_file_name)

reader.vectorise_from_file(osc_file_name,osc_vectoriser)
reader.vectorise_from_file(mouse_file_name,mouse_vectoriser)

print "Input vector OSC:"
print osc_vectoriser.input
print "Output vector OSC:"
print osc_vectoriser.output

print "Input vector mouse:"
print mouse_vectoriser.input
print "Output vector mouse:"
print mouse_vectoriser.output