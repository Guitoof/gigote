__author__ = 'Guillaume'

import src.network.file_recording_management as f_mgmt
import src.network.osc_API as osc_API
import src.network.mouse_API as mouse_API
import os

from sklearn import linear_model
import numpy as np
FS = 20

print os.path.abspath(__file__)
name = 'test_MouseToLive00019'

osc_vectoriser = osc_API.OscVectoriser(FS)
osc_file_name = name + '/' + name + '.osc'

mouse_vectoriser = mouse_API.MouseVectoriser(FS)
mouse_file_name = name + '/' + name + '.mouse'

reader = f_mgmt.Reader()
reader.init_size_vector_size(osc_vectoriser,osc_file_name,mouse_file_name)
reader.init_size_vector_size(mouse_vectoriser,osc_file_name,mouse_file_name)

reader.vectorise_from_file(osc_file_name,osc_vectoriser)
reader.vectorise_from_file(mouse_file_name,mouse_vectoriser)

print "Input vector OSC:"
print osc_vectoriser.input
print "Output vector OSC:"
print osc_vectoriser.output

print "Input vector mouse:"
print mouse_vectoriser.input
print "Output vector mouse:"
print mouse_vectoriser.output

clf = linear_model.LinearRegression()

X = osc_vectoriser.input
Y = osc_vectoriser.output
np.vstack((X,mouse_vectoriser.input))
np.vstack((X,np.ones((1,mouse_vectoriser.n_time))))
np.vstack((Y,mouse_vectoriser.output))

clf.fit (X.T,Y.T)


# The mean square error
print("Residual sum of squares: %.2f"
              % np.mean((clf.predict(X.T) - Y.T) ** 2))
# Explained variance score: 1 is perfect prediction
print('Variance score: %.2f' % clf.score(X.T, Y.T))
